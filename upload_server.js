  const exist = require('@existdb/node-exist')
  const db = exist.connect({
   port: '8080',
   secure: false,
   basic_auth: {
     user: 'admin',
     pass: 'admin'
   }

 })

/**
 * 
 * @param {string} filePath 
 * @param {Buffer} contents 
 * @returns 
 */
async function uploadExistDb (filePath, contents) {
  const fileHandle = await db.documents.upload(contents)
  await db.documents.parseLocal(fileHandle, filePath, {})
  return filePath
}

 function create_collections(path){
    return new Promise( async function (resolve) {
      var result = await db.collections.create(path).then((res) => {return res});
      resolve(result);
     })
 }

 function get_file_collections(path){
  return new Promise( async function (resolve) {
   var result = await db.collections.describe(path).then(function(res){resolve(res.collections)})
   .catch(e => console.error('Cannot get file ', e));
   resolve(result);
  })
 }
module.exports = { uploadExistDb, get_file_collections, create_collections };