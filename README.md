# exist_db

Pushing data into exist db using nodejs

Install the required node module files using below commands
<br>
npm init <br>
npm install @existdb/node-exist<br>
npm install axios<br>

To upload files into exist-db, Kindly place the list of dois in the sample.txt file.<br>
Specify the existDb location in file.js file (Line number - 85)

To run the script, please use the below command in terminal<br>
<br>
node file.js<br>

