const https = require('https');
const upload_server = require("./upload_server");
const fs = require('fs');
const fetch = require("axios");

var statusReport = [];
var logs;

/**
 * Upload xml 
 * @param {string} dois 
 * @param {string} xmlData 
 * @param {string} path 
 * @param {object} status 
 */
function uploadXML(dois,xmlData, path, status,logs){
  upload_server.uploadExistDb(path, Buffer.from(xmlData))
      .then(filePath => {console.log("uploaded - ", filePath)
          status.XMLUpload = 'SUCCESS';
          statusReport.push(status);
          downloadAndUpload(dois, logs);
        })
      .catch(error => {
        status.XMLUpload = 'FAIL';
        fs.writeFile('D:/largexml/FailedDoisXML/'+doi+'.xml', xmlData, function(err) {
          if(err) {
              console.log(err);
          }
          status.XMLFilePath = 'D:/largexml/FailedDoisXML/'+doi+'.xml';
            statusReport.push(status)
            downloadAndUpload(dois,  logs)
      }); 
      })
}

/**
 * 
 * @returns Exits when all dois were uploaded
 */
function readLogs(){
  log = fs.readFileSync('log.json');
  logs = JSON.parse(log);
  var dois = []
   for (var i =0; i<logs.length;i++){
     var status = logs[i];
     if (status.XMLdownloaded == 'FAIL'){
      dois.push(status.doi);
     }
   }
   if(dois.length == 0){
     console.log('all dois were uploaded')
     return 1
   } else {
     downloadAndUpload(dois, logs);
   }
}
var xmlData;
/**
 * Function to upload xml into exist-db
 * @param {string} dois 
 * @param {string} count 
 */
async function downloadAndUpload(dois, logs) {
  if (dois.length != 0){
    doi = dois.shift();
    doi = doi.replace(/(\r\n|\n|\r)/gm, "");
    var project = doi.split('-')[0];
    var status = {
      'doi': doi,
      'XMLdownloaded':'',
      'XMLUpload':'',
      'XMLFilePath':''
    }
    var customer = 'bmj';
    var files = fs.readdirSync('D:/largexml/FailedDoisXML/');
    if(!files.includes(doi+'.xml')){
      xmlData =  await downloadXML(dois, doi, customer, project, status, logs);
      if(xmlData != undefined){
      xmlData = xmlData.data;
      }
    } else {
      xmlData = fs.readFileSync('D:/largexml/FailedDoisXML/'+doi+'.xml',
            {encoding:'utf8', flag:'r'});
    }
    var collectionPath = '/db/kriyadocs/' + customer;
    var existDBCollections = await upload_server.get_file_collections(collectionPath);
    if (!(existDBCollections.includes(project))){
      await upload_server.create_collections(collectionPath + '/' + project)
    }
    path = collectionPath + '/' + project + '/' + doi + '.xml';
    if(xmlData != undefined){
      uploadXML(dois,xmlData,path, status,logs)
    }
  } else {
    if (logs != undefined){
      for (var i =0;i<statusReport.length;i++){
        const searchIndex = logs.findIndex((logIndex) => logIndex.doi==statusReport[i].doi);
        logs[searchIndex] = statusReport[i]
      }
      statusReport = logs;
    }
    var json = JSON.stringify(statusReport);
    fs.writeFileSync('log.json', json)
    statusReport = [];
    readLogs();
  }
}

/**
 *########################################### CODE STARTS HERE ###########################################
 * Read the file containing collection of dois.
 */
fs.readFile('D:/largexml/samples.txt', 'utf8', (err, files) => {
  if (err) {
    console.error('Failed to get file from the location - ', err);
    return;
  }
  var dois = files.split('\n');
  downloadAndUpload(dois);
});

/**
 * Function to download xml data(object)
 * @param {string} doi 
 * @param {string} customer 
 * @param {string} project 
 * @returns object
 */
function downloadXML(dois, doi, customer, project, status, logs) {
  doi = doi.replace('.xml', '');
  var url = (`https://kriya2.kriyadocs.com/resources/${customer}/${project.toLowerCase()}/${doi}/${doi}.xml`);
  url = encodeURI(url)
  var xml =fetch.get(url).then(res => {
    status.XMLdownloaded = 'SUCCESS';
    return res})
  .catch(err => {
    status.XMLdownloaded = "FAIL"
    statusReport.push(status);
    downloadAndUpload(dois, logs)
  })
  return xml
}
